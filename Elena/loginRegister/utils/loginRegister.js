// For Register

$('#btn-register').click((e) => {
    e.preventDefault();

    $.ajax({
        url: "./flux/register.php",
        type: "POST",
        data: {
            email: $("#remail").val(),
            pwd: $("#rpwd").val(),
            pwd2: $("#rpwd2").val(),
        },
        dataType: "json",
        success: (res) => {
            if (res.msg) alert(res.msg);
            console.log(res);
            // document.getElementById("fregister").reset(); //? Reset the form so it does not stay will the same data.

        }
    })
})

// To login
$('#btn-login').click((e) => {
    e.preventDefault();
    $.ajax({
        url: "./flux/login.php",
        type: "POST",
        data: {
            email: $("#lLogin").val(),
            pwd: $("#lpwd").val()
        },
        dataType: "json",
        success: (res, status) => {
            console.log(res);
            if (res.success) {
                localStorage.setItem('mail', JSON.stringify(res.user['email'])); //I stock in my local storage mail is the name I give and email comes from the DB.
                window.location.replace('../../Amir/header.php'); //!
            } else {
                $('#error').text("Your email and password do not match, please try again.");
            }
        }
    })
})