<?php
session_start();
require_once('../../../dbconnexion/db_connect.php');
require_once('../../../dbconnexion/function.php');

if (isset($_POST['email'], $_POST['pwd'], $_POST['pwd2'])) {
    $email = mysqli_real_escape_string($db, $_POST['email']);
    $pwd = mysqli_real_escape_string($db, $_POST['pwd']);
    $pwd2 = mysqli_real_escape_string($db, $_POST['pwd2']);

    // Verify the email format
    $regex = "/^[^0-9][_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/";
    if (!preg_match($regex, $email)) {
        echo json_encode(['success' => false, 'msg' => 'The email does not have the correct format']);
        die();
    }
    // Verify the the password is the same
    if ($pwd != $pwd2) {
        echo json_encode(['success' => false, 'msg' => 'The passwords do not match']);
        die();
    }
    //! Security -> hash password
    $hash = password_hash($pwd, PASSWORD_DEFAULT);

    $sql = "INSERT INTO user (email, pwd) VALUES ('{$email}','{$hash}')";
    $db->query($sql);

    echo json_encode(['success' => true]);
} else echo json_encode(['success' => false]);
