<?php
session_start();

if (!isset($_SESSION['connected']) || isset($_GET['disconnect'])) {
    $_SESSION['connected'] = false;
    $_SESSION['user'] = [];
}

if (isset($_POST['connected'])) {
    $_SESSION['connected'] = true;
    $_SESSION['login'] = $_POST['login'];
}

require_once('../../../dbconnexion/db_connect.php');
require_once('../../../dbconnexion/function.php');

if (isset($_POST['email'], $_POST['pwd'])) {

    //! Verify the password is hashed
    // we have to select everything that we want to send to the localStorage or to $_SESSION
    $sql = "SELECT * FROM user WHERE email = '{$_POST['email']}'";
    // echo $sql;

    $res = $db->query($sql);
    $user = resultAsArray($res)[0];

    if (password_verify($_POST['pwd'], $user['pwd'])) {
        // echo ('Im in!');
        // THE SUPERGLOBAL $_SESSION is only for id while the localStorage can be used for other information
        $_SESSION['user'] = [
            'id' => $user['iduser']
        ];
        // ? Arrays use var_dump not echo
        // var_dump($_SESSION['user']);
        if (count($user)) {
            echo json_encode(['success' => true, 'user' => $user]);
        } else echo json_encode(['success' => false]);
    } else echo json_encode(['success' => false]);
} else echo json_encode(['success' => false]);
