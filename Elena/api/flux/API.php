<?php
// echo "hello";

session_start();
require_once('../../../dbconnexion/db_connect.php');
require_once('../../../dbconnexion/function.php');

//? This will be used for the history (I will not have time)
// $ville = $_POST["name"];
// // echo $ville;

// $temp = $_POST["temperature"];
// // echo $temp;

// $datetime = date_create()->format('Y-m-d H:i:s');
// // before  you need to insert into table city if id does not exist create else add.
// if (isset($ville, $temp)) {
//     $sql = "INSERT INTO `history` (time, city, weatherDegrees) VALUES ('{$datetime}', '{$ville}', '{$temp}')";
//     $repSQL = $db->query($sql);

//     echo json_encode(['success' => true]);
// } else echo json_encode(['success' => false]);


if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $method = $_POST;
} else {
    $method = $_GET;
}

switch ($method['choice']) {

        // case 'select_all': //? It is in the city.php
        //     $sql = "SELECT * FROM city";
        //     $resSQL = $db->query($sql);
        //     $city = resultAsArray($resSQL);
        //     echo json_encode($city);
        //     break;

    case 'select_id':
        if (isset($method['cityId'])) {
            $idCity = mysqli_real_escape_string($db, $method['cityId']);
            $sql = "SELECT * FROM city WHERE idcity = {$idCity}"; // Select one category according to the id
            $resSQL = $db->query($sql);
            $city = resultAsArray($resSQL)[0];
            echo json_encode(['success' => true, 'city' => $city]); //Send success true and the city data
        } else echo json_encode(['success' => false]);
        break;

    case 'insertCity':
        if (isset($method['name'])) {
            // if nameCity is the same than another nameCity in the table City we do not insert a new row.
            $nameCity = mysqli_real_escape_string($db, $method['name']);
            $sql = "INSERT INTO `city` (`nameCity`) VALUES ('{$nameCity}')";
            $resSQL = $db->query($sql);
            echo json_encode(['success' => true]);
        } else {
            echo json_encode(['success' => false]);
        }
        break;

    case 'favorite':
        // Once I activate my function I send it via 'addFavorite' I open a new window and send the idcity and iduser
        if (isset($method['idCity'], $_SESSION['user']['id'])) {
            $idCity = mysqli_real_escape_string($db, $method['idCity']);
            // $sql = "SELECT * FROM `usercity` WHERE `iduser`= '{$_SESSION['user']['id']}' AND `idcity`=$idCity ;";
            $sql = "SELECT nameCity.c, *.uc 
            FROM city c
            INNER JOIN usercity uc WHERE `iduser`= '{$_SESSION['user']['id']}' AND `idcity`=$idCity
            ON c.idcity = uc.idcity
            ";
            $resSQL = $db->query($sql);
            echo json_encode(['success' => true, 'idcity' => $idCity, 'iduser' => $_SESSION['user']['id']]);
        } else {
            echo json_encode(['success' => false]);
        }
        break;
}
