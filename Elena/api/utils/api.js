//? I will need this for the history
// const urlParams = new URLSearchParams(window.location.search); // To pass the id through the link using js (from jewelry.js)
// const idProduct = urlParams.get('id');
// // console.log(id);

function addFavorite(idCity) {
    // console.log(idCity);
    // Once we click on the button add to favorites we insert in the page favorites.html (also need to verify how to change from 1 to 0).
    $.ajax({
        url: "./flux/api.php",
        type: "GET",
        data: {
            choice: 'favorite',
            idCity: idCity
        },
        dataType: 'json',
        success: (res, status) => {
            console.log(res['success'])
            if (res.success) {
                // window.location.replace("./favorites.html?idcity=" + res['idcity']); //! I do not have time for this, so it will be on the same page.

                $('#tablefavorite').append(
                    "<tr id='tr-" + res.idcity + "'>" +
                    "<td id='td-name-" + res.idcity + "'>" + res.nameCity + "</td>" +
                    // Here I send the id onclick to the function, so once it is clicked the id is already there.
                    "<td><button onclick='delete(" + res.idcity + ")'>Delete</button></td>" +
                    "</tr>");
                console.log($('#tablefavorite'));
            }

        }
    })
}

function initial() {
    // 1. SELECT I definde my table structure that I select everything from before inserting, using a function.
    $.ajax({
        url: "./flux/city.php",
        type: "GET",
        data: {
            choice: 'select_all' // A select option is always in get except for delicate data.
        },
        dataType: "json",
        success: (cities, res) => {
            // console.log(res);
            // We create a table connected to the DB via id
            let html = '';
            cities.forEach(city => {
                html +=
                    "<tr id='tr-" + city.idcity + "'>" +
                    "<td id='td-name-" + city.idcity + "'>" + city.nameCity + "</td>" +
                    "<td><button onclick='addFavorite(" + city.idcity + ")'>Ajouter Au Favoris</button></td>" +
                    "</tr>"
            });
            console.log(html);
            $('#tablecity').append(html);
        }
    });
}

initial();

$("#btn-api").click((e) => {
    e.preventDefault();

    const idCity = $("#cityId").val();
    const name = $("#nameapi").val();

    let city = document.getElementById('nameapi').value;
    // console.log(city);

    let url = "http://api.openweathermap.org/data/2.5/weather?q=" + city + "&lang=fr&units=metric&appid=a6be8bcff7b226f7f1864379c72a6af1";
    // console.log(url);

    $.get(url, function(data) {
        // console.log(data);
        $('#addAPI').append(
            "<ul>" +
            "<li>" + data.name + "</li>" +
            "<li>" + data.main.temp + "°C</li>" +
            "</ul>");
        if (idCity.trim() == '') {
            $.post("./flux/api.php", {
                    choice: 'insertCity',
                    name: data.name,
                    temperature: data.main.temp
                },
                function(res) {
                    // console.log(res);
                    // if (res.success) { 
                    //! It does not go inside using the 'if', why? Even though it is successful
                    // console.log('hello');
                    $('#tablecity').append(
                        "<tr id='tr-" + res.idcity + "'>" +
                        "<td id='td-name-" + res.idcity + "'>" + data.name + "</td>" +
                        // Here I send the id onclick to the function, so once it is clicked the id is already there.
                        "<td><button onclick='addFavorite(" + res.idcity + ")'>Ajouter Au Favoris</button></td>" +
                        "</tr>");
                    console.log($('#tablecity'));
                    // } else alert("Error inserting data");

                    document.getElementById("formapi").reset();
                });

        }
    });
})