<?php
require 'db.php';
$get_member = $connect->prepare('SELECT * FROM user WHERE email = ?');
$fetch_member = $get_member->fetch();

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Météo</title>
  <link rel="icon" type="image/png" href="images/icons/favicon.ico">
  <link rel="stylesheet" href="./CSS/styles.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.0/css/all.css" />
</head>

<body>

  <img src="./Pics/image.png" width="100" height="100">
  <header>
    <nav>

      <div class="onglets">
        <ul>
          <li><a href="Historique.php">Historique</a>
            <!-- //! Chage to favorites.html -->
          <li><a href="./..\Elena\api\api.html">Recherche</a>
          <li><a href="compte.php">Compte</a>
          <li><a href="./..\Elena\loginRegister\loginRegister.html">Inscription</a>
          <li><a href=".\..\Elena\loginRegister\loginRegister.html">Connexion</a>
          <li><a href="deconnexion.php">Déconnexion</a>
        </ul>

        <?php if (isset($_SESSION['email'])) {
          if ($fetch_member['Perm'] == 1) { ?>
            <p><a href="Supprimer.php"><i class="fas fa-users-cog"></i></a></p>
        <?php }
        } ?>
      </div>
    </nav>
  </header>

  <!-- Fin de la barre de navigation -->

  <!-- Header -->
  <header>
    <h1>Bienvenue</h1>
  </header>
  <?php
  require 'api.php';
  ?>