<?php
session_start();
$connect = new pdo("mysql:host=localhost;dbname=meteo_project_bdd_EV_AC.sql;charset=utf8", "root", "");

$pdoStat = $connect->prepare("SELECT * FROM user WHERE iduser = :iduser");
$pdoStat->bindValue(':iduser', $_SESSION['user']['id']);
$executeIsOk = $pdoStat->execute();
$user = $pdoStat->fetch();

?>
<!DOCTYPE html>
<html>

<head>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>

</head>

<body>

    <div class="album py-5 bg-light">
        <div class="container">

            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">

                <form action="modifier.php" method="post">
                    <div class="mb-3">
                        <label for="email" class="form-label">email</label>
                        <input type="text" class="form-control" value="<?= $user['email']; ?>" name="email" required>
                    </div>

                    <div class="mb-3">
                        <label for="password" class="form-label">password</label>
                        <input type="text" class="form-control" name="pwd" required>
                    </div>
                    <input value="Modifier Le Profil" type="submit" name="Modifier" class="btn btn-primary">
                </form>

            </div>
        </div>
    </div>


</body>

</html>