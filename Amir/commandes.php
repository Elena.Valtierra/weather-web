<?php
$connect = new pdo("mysql:host=localhost;dbname=meteo_project_bdd_EV_AC.sql;charset=utf8", "root", "");
function ajouter($email, $pwd)
{
    try {

        global $connect;
        $req = $connect->prepare("INSERT INTO user (email, pwd) VALUES (?,?)");
        $req->execute(array($email, $pwd));
    } catch (PDOException $err) {

        echo $err->getMessage();
    }
}

function afficher()
{
    global $connect;
    $req = $connect->prepare("SELECT * FROM user ORDER BY iduser ASC");

    $req->execute();

    $data = $req->fetchAll(PDO::FETCH_OBJ);

    return $data;
}
