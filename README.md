# Weather Web

Project Météo

//Pour ce projet nous avons utiliser:
+ -HTML
+ -CSS
+ -PHP
+ -JavaScript
+ -SQL

//Description :
Ce projet a pour but de creer un site web qui permet à l'utilisateur de consulter la météo de chaque ville en temps réel.
Pour ce fait nous avons récupéré la météo grâce à l'API d'OpenWeatherMap et nous avons creer un espace membre ainsi qu'un moyen d'ajouter une météo préférer.
Nous avons tout d'abord commencé la conception du site en utilisant Star Uml ainsi que MYSql Work Bench, s'en suit la création des pages web en PHP pour pouvoir requêter la base de données et récupérer les informations inscrites ainsi que la modification des données sur l'espace membre.
Après avoir terminé le PHP nous sommes passé au JS pour pouvoir ajouter la météo dans un onglet favoris.

Consigne:
pour faire fonctionner le site il faut:
+ Décompresser le fichier Rar dans le Local Host.
+ Récupérer et exécuter le fichier Sql dans PHPmyadmin.
+ Le fichier header.php est la source principale de l'interface.
+ Allez dans Xamp/Wamp et démarrer la session.



A weather website designed to see the temperature of different cities and save them as favorites.
It uses an API.
